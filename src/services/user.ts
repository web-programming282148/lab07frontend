import type { User } from '@/types/User'
import http from './http'

async function addUser(user: User) {
  const res = await http.post('/users', user)
}

async function updateUser(user: User) {
  const res = await http.patch('/users/' + user.id, user)
}

async function delUser(user: User) {
  const res = await http.delete('/users/' + user.id)
}

function getUser(id: number) {
  return http.get('/users/' + id)
}

function getUsers() {
  return http.get('/users')
}

export default { addUser, updateUser, delUser, getUser, getUsers }
