import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import userService from '@/services/user'
import type { User } from '@/types/User'
import { ref } from 'vue'

export const useUserStore = defineStore('user', () => {
  const useLoading = useLoadingStore()
  const users = ref<User[]>([])

  async function saveUser(user: User) {
    useLoading.doLoad()
    if (user.id < 0) {
      //add user
      const res = await userService.addUser(user)
    } else {
      //update user
      const res = await userService.updateUser(user)
    }
    await getUsers()
    useLoading.finishLoad()
  }

  async function delUser(user: User) {
    const res = await userService.delUser(user)
  }

  async function getUsers() {
    useLoading.doLoad()
    const res = await userService.getUsers()
    users.value = res.data
    useLoading.finishLoad()
  }

  async function getUser(id: number) {
    useLoading.doLoad()
    const res = await userService.getUser(id)
    users.value = res.data
    useLoading.finishLoad()
  }
  return { users, getUser, getUsers, delUser, saveUser }
})
