import { ref } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import temperatureService from '@/services/temperature'

export const useTemperatureStore = defineStore('temperature', () => {
  const valid = ref(false)
  const celsius = ref(0)
  const result = ref(0)
  const useLoading = useLoadingStore()

  function convert(celsius: number): number {
    return (celsius * 9.0) / 5 + 32
  }

  async function callConvert() {
    useLoading.doLoad()
    // result.value = convert(celsius.value)
    try {
      result.value = await temperatureService.convert(celsius.value)
    } catch (e) {
      console.log(e)
    }
    useLoading.finishLoad()
  }

  return { valid, celsius, result, convert, callConvert }
})
